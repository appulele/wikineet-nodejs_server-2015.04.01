var express = require('express');
var mysql = require('mysql');
 
var iconv = require('iconv-lite');

var router = express.Router();


var cheerio = require('cheerio');
var request = require('request'); 
var fs = require('fs');

var querystring = require('querystring');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/users', function(req, res, next) {
  var client = mysql.createConnection({
    user:'root',
    password:'green5569',
    database:'wikineet_user'
  });

  var users;
  client.query('USE wikineet_user');

  
  //TODO 오라클, sql, pl/sql,쿼리문 쪽 공부해서 전체 불러오기 말고 특정 부분 불러오기 
  client.query('SELECT * FROM wikineet_user', function (error, result, fields) {
      if (error) {
        console.log("쿼리문 오류1");
      }else{
         
        console.log(result);
        users = result;
        console.log("query result=====================");
 		res.render('index', { title: 'Express' , users: users});
         
        
      };
    });

  
}); 
 
var BASE_URL = "http://namu.wiki/"
var namuFind = "http://namu.wiki/search/";
var namuMove = "http://namu.wiki/w/"
var namuRandom = "http://namu.wiki/random";

 
var namuHead = ""; 
try{
	namuHead = fs.readFileSync('public/html/namuHead.html', 'utf8'); 
}catch(e){ 
	alert("Error");
}
 

router.get('/:text(*)', function(req, res, next) { 
  
  var text =  BASE_URL + encodeURIComponent(req.params.text);	
  text = text.replace("%2F","/");

  var strParam = req.params[0];
  var diffFlag = false;

  try{
  	var diff = strParam.slice(0,4);
  	if (diff == "diff" || diff =="edit") 	{
  		text = text + "?" +querystring.stringify(req.query);
  		diffFlag = true;
  	}


  }catch(e){
  	 
  }
  request(text,function (error,response,body) {
  	

  	if(diffFlag){
  		res.redirect(text);
  	}else{
  		try{  
		body = body.replace("</head>", namuHead);  
		res.send(body); 	
		
		// res.send(text);		 
	}catch(e){
		console.error("catch, file read error.");
		res.send(text + ":error");
	}
	
  	}
  		
  	
  });   
});




//나무위키
router.get('/w/:text(*)', function(req, res, next) { 
  
  var text = namuMove + encodeURIComponent(req.params.text);	
  
  request(text,function (error,response,body) {
  	try{  
		body = body.replace("</head>",namuHead);  
		res.send(body); 
	}catch(e){
		console.error("catch, file read error.");
	}
  });   
});

 
router.get('/go/:text(*)', function(req, res, next) {
	var text = namuFind + encodeURIComponent(req.params.text);	
	// var text = enhaFind + encodeURIComponent(req.params.text);	
  	request(text,function (error,response,body) {
  	
  	try{
  		console.log("text:"+req.params.text); 
	  	// var fixedHead = fs.readFileSync('public/html/fixedHead.html', 'utf8');
	  	 
		body = body.replace("</head>",namuHead);  
		// res.render('index', { title: 'WIKIPIDIA FIND  ,' +body});
		// res.render(body);

		res.send(body); 
	}catch(e){
		console.error("catch, file read error.");
	}
  });   
});


router.get('/search/:text(*)', function(req, res, next) {
	var text = namuFind + encodeURIComponent(req.params.text);	
	// var text = enhaFind + encodeURIComponent(req.params.text);	
  	request(text,function (error,response,body) {
  	
  	try{
  		console.log("text:"+req.params.text); 
	  	body = body.replace("</head>",namuHead);   

		res.send(body); 
	}catch(e){
		console.error("catch, file read error.");
	}
  });   
});
   
 
//random
router.get('/random/:text(*)', function(req, res, next) {
	var type = req.params.text;
	 
	var url = enhaRandom;
	var head = fixedHead;


	console.log("!!  TYPE IS  !!"+type);

	if (type == "namu") {
		url = namuRandom;
		head = namuFixedHead;
		console.log("type is namu!!!!!!!"+url);
	}else if (type == "wiki") {
		
		url = wikiRandom;
		head = fixedWikiHead;
		console.log("type is wiki!!!!!!!"+url);
	}
	else if (type == "enha") { 
		//임시로 엔하 랜덤은 나무 랜덤으로 처리 
		url = namuRandom;
		head = namuFixedHead;
		console.log("type is mirror!!!!!!!");
	}else{

	}
  request(url,function (error,response,body) {
  	try{    
	  body = body.replace("</head>",head);  
	  res.send(body); 
	
	}catch(e){
		console.error("catch, file read error.");
	}
  });   
});


router.get('/reverse_namu/:text(*)', function(req, res, next) {
	 
	var text = req.params.text;
	
	 
		url = namuReverse+text;
		head = namuFixedHead;
		console.log("type is namu!!!!!!!"+url);
	  
	 
  request(url,function (error,response,body) {
  	try{    
	  body = body.replace("</head>",head);  
	  res.send(body); 
	
	}catch(e){
		console.error("catch, file read error.");
	}
  });   
});


router.get('/reverse_enha/:text(*)', function(req, res, next) {
	 
	var text = req.params.text;
	
	 
	 
		url = enhaReverse+text;
		head = namuFixedHead;
		console.log("type is mirror!!!!!!!");
	 
  request(url,function (error,response,body) {
  	try{    
	  body = body.replace("</head>",head);  
	  res.send(body); 
	
	}catch(e){
		console.error("catch, file read error.");
	}
  });   
});





//나무 에디트 리다이렉트 
router.get('/edit/:text(*)', function(req, res, next) {
   res.redirect("https://namu.wiki/edit/"+req.params.text);
});


router.get('*', function(req, res, next) {
  res.render('index', { title: 'Home' });
});

module.exports = router;


 